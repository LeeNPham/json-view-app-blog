from django.http import JsonResponse
from .models import Blog, Comment
from common.json import ModelEncoder
import json
from django.views.decorators.http import require_http_methods

class CommentListEncoder(ModelEncoder):
    model = Comment
    properties = ["content"]

class BlogListEncoder(ModelEncoder):
    model = Blog
    properties = ["title"]

class BlogDetailEncoder(ModelEncoder):
    model = Blog
    properties = ["title", "content",]

@require_http_methods(["GET", "POST"])
def list_blogs(request):
    if request.method == "GET":
        blogs = Blog.objects.all()
        return JsonResponse({"blogs": blogs}, encoder=BlogListEncoder)
    else: 
        content = json.loads(request.body)
        blog = Blog.objects.create(**content)
        return JsonResponse({"blog": blog}, encoder=BlogListEncoder)


@require_http_methods(["GET", "PUT", "DELETE"])
def show_blog_detail(request, pk):
    if request.method == "GET":
        blog = Blog.objects.get(pk=pk)
        return JsonResponse({"blog": blog}, encoder=BlogDetailEncoder)
    elif request.method == "DELETE":
        count, _ = Blog.objects.filter(id=pk).delete()
        print("count: ",count)
        print("_", _)
        return JsonResponse({"delete": count > 0})
    else:
        content = json.loads(request.body)
        Blog.objects.filter(id=pk).update(**content)
        blog = Blog.objects.get(id=pk)
        return JsonResponse({"Updated": blog}, encoder=BlogDetailEncoder)

def create_comment(request, blog_id):
    if request.method == "POST":
        blog = Blog.objects.get(id=blog_id)
        content = json.loads(request.body)
        Comment.objects.create(
            content=content["comment"],
            blog=blog,
        )
        return JsonResponse({"message": "created"})
